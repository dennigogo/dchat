echo ''
echo '============================================'
echo 'Domain list:'
echo 'demo: http://demo.dchat.int'
echo 'api: http://api.dchat.int'
echo 'admin: http://admin.dchat.int'
if [ ${INSTALL_NGINX} = true ]; then
    if [ ${INSTALL_PHP7} = true ]; then
        if [ ${INSTALL_MARIADB} = true ]; then
            echo 'mariadb: http://mariadb.dchat.int (user: "dchat" , password: "dchat"; or "#sudo mysql -u root" from terminal)'
        fi
        if [ ${INSTALL_REDIS} = true ]; then
            echo 'redis: http://redis.dchat.int'
        fi
    fi
fi
if [ ${INSTALL_MONGODB} = true ]; then
    echo 'mongodb: http://dchat.int:3000 (dbName as "db_dchat" and admin as: u "root" p "root"; readWrite as: u "dchat" p "dchat")'
fi
echo '============================================'
echo ''
