#!/usr/bin/env bash

# set locale 
# sudo update-locale LC_ALL=C.UTF-8
# Set non-interactive mode
export DEBIAN_FRONTEND=noninteractive


# abort on nonzero exitstatus
#set -o errexit
# abort on unbound variable
#set -o nounset
# don't mask errors in piped commands
#set -o pipefail

# Color definitions
readonly reset='\e[0m'
readonly cyan='\e[0;36m'
readonly red='\e[0;31m'
readonly yellow='\e[0;33m'	


info() {
 printf "${cyan}>>> %s${reset}\n" "${*}" 
}


if [ ! -e /home/dchat/vagrant/.provision ];
then
	
    info "updating system packages"
    sudo apt-get update --fix-missing
    sudo apt-get --yes upgrade
    #sudo apt-get dist-upgrade
    echo "."

    info "installing essentials and tools ..."
    sudo apt-get install --yes software-properties-common
    sudo apt-get install --yes python-software-properties
    sudo apt-get install --yes wget curl htop vim nano
    sudo apt-get install --yes ssh
    sudo apt-get install --yes mc
    sudo apt-get install --yes git git-flow
    sudo apt-get install --yes docker.io
	echo "."

    if [ ${INSTALL_NGINX} = true ]; then
        info "installing lastest stable Nginx"
        sudo add-apt-repository -y ppa:nginx/stable
        sudo apt-get update > /dev/null
        sudo apt-get install -qq nginx
        echo "."
    fi

    #--------------------------------------
    # Php 7 developpement
    #-------------------------------------

    if [ ${INSTALL_PHP7} = true ]; then
        info "Installing php 7.1"
        sudo add-apt-repository ppa:ondrej/php
        sudo apt-get update > /dev/null
        sudo apt-get install -qq php7.1-cli php7.1-common php7.1-curl php7.1-fpm php7.1-gd php7.1-intl  php7.1-mbstring php7.1-mcrypt php7.1-mysql php7.1-opcache php7.1-xml php7.1-xmlrpc php7.1-zip php-imagick php7.1-dev
        sudo cp /home/dchat/vagrant/php-fpm/php-fpm.service /etc/systemd/system
        sudo cp -f /home/dchat/vagrant/php-fpm/pool.d/www.conf /etc/php/7.1/fpm/pool.d
        sudo systemctl start php-fpm
        sudo systemctl status php-fpm
        sudo systemctl enable php-fpm
        sudo apt-get install --qq libcurl4-openssl-dev pkg-config libssl-dev libsslcommon2-dev
        echo "."

        info "installing Composer"
        cd /tmp
        sudo curl -sS https://getcomposer.org/installer | php
        sudo mv composer.phar /usr/local/bin/composer
        sudo composer global require "fxp/composer-asset-plugin:^1.2.0"
        echo "."
		
        info "installing PHPUnit 6.4.4"
        cd /tmp
        wget https://phar.phpunit.de/phpunit-6.4.4.phar
        sudo chmod +x phpunit-6.4.4.phar
        sudo mv phpunit-6.4.4.phar /usr/local/bin/phpunit
        echo "."
		
        info "Installing XDebug"
        pecl install xdebug
        sudo echo "zend_extension=xdebug.so" > /etc/php/7.1/mods-available/xdebug.ini
        sudo ln -sf /etc/php/7.1/mods-available/xdebug.ini /etc/php/7.1/fpm/conf.d/20-xdebug.ini
        sudo ln -sf /etc/php/7.1/mods-available/xdebug.ini /etc/php/7.1/cli/conf.d/20-xdebug.ini
        sudo service php-fpm restart
        echo "."
    fi

    #--------------------------------------
    # nodejs
    #-------------------------------------
	
    if [ ${INSTALL_NODEJS} = true ]; then
        info "installing NodeJS"
        sudo apt-get install -y nodejs-legacy
        sudo apt-get install -y npm
        sudo npm cache clean -f
        sudo npm install -g n
        sudo n latest
        sudo npm install -g nave
        sudo nave use 6.11.1
        echo "."
    fi

    #--------------------------------------
    # database
    #-------------------------------------

    if [ ${INSTALL_MARIADB} = true ]; then
        info "installing MariaDB"
        sudo apt-get install -y --force-yes software-properties-common
        sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
        sudo apt-get update -y > /dev/null
        sudo apt-get -y --force-yes install mariadb-server
        sudo mysql -uroot -e "CREATE USER 'dchat'@'localhost' IDENTIFIED BY 'dchat';"
        sudo mysql -uroot -e "CREATE DATABASE db_dchat CHARACTER SET utf8 COLLATE utf8_general_ci;"
        sudo mysql -uroot -e "GRANT ALL PRIVILEGES ON db_dchat.* TO 'dchat'@'localhost';"
        if [ ${INSTALL_PHP7} = true ]; then
            sudo mkdir /var/www/phpMyAdmin
            sudo composer create-project -s stable phpmyadmin/phpmyadmin /var/www/phpMyAdmin
            if [ ${INSTALL_NGINX} = true ]; then
                sudo chown -R www-data:www-data /var/www/phpMyAdmin
                sudo ln -sf /home/dchat/vagrant/nginx/phpMyAdmin.conf /etc/nginx/sites-enabled/phpMyAdmin
                sudo systemctl restart nginx
                sudo systemctl status nginx
            fi
        fi
	fi

    if [ ${INSTALL_REDIS} = true ]; then
        info "installing Redis"
        sudo apt-get update -y > /dev/null
        sudo apt-get -y --force-yes install build-essential tcl
        cd /tmp
        sudo curl -O http://download.redis.io/redis-stable.tar.gz
        sudo tar xzvf redis-stable.tar.gz
        cd redis-stable
        make
        make test
        sudo make install
        sudo mkdir /etc/redis
        sudo cp /tmp/redis-stable/redis.conf /etc/redis
        sudo cp /home/dchat/vagrant/redis/redis.service /etc/systemd/system
        sudo adduser --system --group --no-create-home redis
        sudo mkdir /var/lib/redis
        sudo chown redis:redis /var/lib/redis
        sudo chmod 770 /var/lib/redis
        sudo sed -i -e '$a\
dir /var/lib/redis' /etc/redis/redis.conf
        sudo systemctl enable redis
        sudo systemctl restart redis
        if [ ${INSTALL_PHP7} = true ]; then
            sudo apt install --yes php-redis
            sudo service php-fpm restart
            sudo mkdir /var/www/phpRedisAdmin
            sudo composer create-project -s stable erik-dubbelboer/php-redis-admin /var/www/phpRedisAdmin
            if [ ${INSTALL_NGINX} = true ]; then
                sudo chown -R www-data:www-data /var/www/phpRedisAdmin
                sudo ln -sf /home/dchat/vagrant/nginx/phpRedisAdmin.conf /etc/nginx/sites-enabled/phpRedisAdmin
                sudo systemctl restart nginx
                sudo systemctl status nginx
            fi
        fi
    fi

    if [ ${INSTALL_MONGODB} = true ]; then
        info "installing MongoDB"
        sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
        echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
        sudo apt-get update -y > /dev/null
        sudo apt-get install -y --force-yes mongodb-org
        sudo systemctl start mongod
        sudo systemctl status mongod
        sudo systemctl enable mongod
        sudo mongo localhost:27017/admin /home/dchat/vagrant/mongodb/mongoAdmin.js
        sudo mongo localhost:27017/db_dchat /home/dchat/vagrant/mongodb/mongoUser.js
        sudo sed -i -- 's/#security:/security:\n    authorization: enabled/g' /etc/mongod.conf
        sudo sed -i -- 's/  bindIp: 127.0.0.1/#  bindIp: 127.0.0.1/g' /etc/mongod.conf
        sudo service mongod restart
        if [ ${INSTALL_PHP7} = true ]; then
            sudo apt-get install --yes php7.1-mongo
            sudo service php-fpm restart
        fi
        sudo docker pull mongoclient/mongoclient
    fi

    #--------------------------------------
    # other stuff
    #-------------------------------------

    if [ ${INSTALL_GOLANG} = true ]; then
        info "installing Google Golang 1.8"
        cd /tmp
        sudo curl -O https://storage.googleapis.com/golang/go1.8.linux-amd64.tar.gz
        sudo tar -xvf go1.8.linux-amd64.tar.gz
        sudo mv go /usr/local
        echo 'export PATH="$PATH:/usr/local/go/bin"' >> ~/.profile
        source ~/.profile
    fi

    #--------------------------------------
    # configure fixes, clean and done
    #-------------------------------------

    #configure locales
    export LANGUAGE=en_US.UTF-8
    export LANG=en_US.UTF-8
    export LC_ALL=en_US.UTF-8
    locale-gen en_US.UTF-8
    dpkg-reconfigure locales

    #clean up
    sudo apt-get autoremove > /dev/null
    sudo apt-get autoclean > /dev/null
    sudo apt-get clean > /dev/null

    touch /home/dchat/vagrant/.provision
    info "Provisioning done."
fi