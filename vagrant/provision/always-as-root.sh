if [ ${INSTALL_NGINX} = true ]; then
    if [ ${INSTALL_PHP7} = true ]; then
        if [ ${INSTALL_MARIADB} = true ]; then
            sudo chown -R www-data:www-data /var/www/phpMyAdmin
            sudo ln -sf /home/dchat/vagrant/nginx/phpMyAdmin.conf /etc/nginx/sites-enabled/phpMyAdmin
        fi
        if [ ${INSTALL_REDIS} = true ]; then
            sudo chown -R www-data:www-data /var/www/phpRedisAdmin
            sudo ln -sf /home/dchat/vagrant/nginx/phpRedisAdmin.conf /etc/nginx/sites-enabled/phpRedisAdmin
        fi
    fi
    service nginx restart
    service php-fpm restart
fi
if [ ${INSTALL_MONGODB} = true ]; then
    docker run -d -p 3000:3000 mongoclient/mongoclient
fi